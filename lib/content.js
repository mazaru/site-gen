import fs from 'fs'
import path from 'path'
import remark from 'remark'
import html from 'remark-html'
import simpleGit from 'simple-git'
import matter from 'gray-matter'
import markdown from 'remark-parse'
import { createFeed } from './feed'

let git = simpleGit( { baseDir: path.join(process.cwd(), '..') })
let essaysDir = path.join(process.cwd(), '..', 'essays')
if (!fs.existsSync(essaysDir)) {
  essaysDir = path.join(process.cwd(), 'essays')
  git = simpleGit()
}
const publicDir = path.join(process.cwd(), 'public')

export async function metadata() {
  const filepath = path.join(process.cwd(), 'README.md')
  const contents = fs.readFileSync(filepath, 'utf8')
  const matterResult = matter(contents)
  return matterResult.data
}

export async function mdToHtml(filepath) {
	const contents = fs.readFileSync(filepath, 'utf8')
	const matterResult = matter(contents)
	const processedContent = await remark()
		.use(html)
		.process(matterResult.content)
	return processedContent.toString()
}

export async function init() {
  const meta = await metadata()
  const latestEssays = await getLatestEssays()
	await createFeed({ latestEssays, publicDir, meta })
}

async function processEssays() {
	const essays = fs.readdirSync(essaysDir)
	const allEssays = essays.map(async fn => {
		const id = fn.replace(/\.md$/, '')
		const fullPath = path.join(essaysDir, fn)
		const contents = fs.readFileSync(fullPath, 'utf8')
		const matterResult = matter(contents)

		const words = (matterResult.content.match(/ /g) || []).length
		const min = Math.ceil(words / 280)

		const log = await git.log([`${essaysDir}/${id}.md`])
		const lastUpdate = log.latest?.date
		const date = log.all[log.all.length - 1]?.date || new Date().toISOString()

		const tree = await remark()
			.use(markdown)
			.parse(matterResult.content)

		const processedContent = await remark()
			.use(html)
			.process(matterResult.content)

		const title = tree.children.find(n => n.type === 'heading').children[0].value
		const content = matterResult.content
		const contentHtml = processedContent.toString()

		return {
			id, date, min, title, content, contentHtml, ...matterResult.data
		}
	})
	return await Promise.all(allEssays)
}

export async function getAllEssayIds() {
	return (await processEssays()).map(pd => {
		return {
			params: {
				id: pd.id
			}
		}
	})
}

export async function getEssay(id) {
	return (await processEssays()).filter(p => p.id === id)[0]
}

export async function getLatestEssays() {
	return (await processEssays()).sort((a,b) => (a.date < b.date) ? 1 : -1)
}
