import fs from 'fs'
import path from 'path'
import { Feed } from 'feed'

export function createFeed({ latestEssays, publicDir, meta }) {
  const { title: blogTitle, email } = meta;
  const url = `https://${blogTitle}`
	const feed = new Feed({
    title: blogTitle,
		//description: 'Feed description',
    id: url,
    link: url,
    favicon: '/favicon.ico',
    author: {
      name: blogTitle,
      email,
      //link
    },
    feedLinks: {
      atom: `${url}rss.xml`,
    },
		/*feedLinks: { 
			json: 'https://example.com/json',
			atom: 'https://example.com/atom',
		},*/
	});

	latestEssays.forEach(essay => {
    const { id, date, title, content } = essay;
		feed.addItem({
			title,
      description: content,
      id: `${url}/essays/${id}`,
      link: `${url}/essays/${id}`,
			//content,
      author: [
        {
          name: blogTitle,
          email,
          //link
        }
      ],
      date: new Date(date),
		})
	})

	const feedFile = path.join(publicDir, 'rss.xml')
	fs.writeFileSync(feedFile, feed.rss2())
}
