import Head from 'next/head'
import Link from 'next/link'
import Layout from '../../components/layout'
import Date from '../../components/date'
import styles from './essay.module.scss'
import { getAllEssayIds, getEssay, metadata } from '../../lib/content'

export async function getStaticProps({ params }) {
	return {
		props: {
			essay: await getEssay(params.id),
      meta: await metadata()
		}
	}
}

export async function getStaticPaths() {
	return {
		paths: await getAllEssayIds(),
		fallback: false
	}
}

export default function Essay({ essay, meta }) {
	return (
		<Layout title={`← ${meta.title}`}>
			<Head>
				<title>{essay.title}</title>
			</Head>
			<article className="h-entry">
        <div className="p-name">{essay.title}</div>
        <div className="u-author h-card" href="/" />
        <div className="u-url" href="." />
				<div className={styles.header}>
					<Date className="dt-published" dateStr={essay.date} />
					<p>{essay.min} min read</p>
				</div>
				<div className="e-content" dangerouslySetInnerHTML={{ __html: essay.contentHtml }} />
			</article>
      <br />
      <br />
      <div className={styles.footer}>
        <Link href='/'>
            <a>← back to essays</a>
        </Link>
      </div>
		</Layout>
	)
}

