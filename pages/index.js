import Head from 'next/head'
import Link from 'next/link'
import Date from '../components/date'
import Layout from '../components/layout'
import { init, getLatestEssays, metadata } from '../lib/content'

export async function getStaticProps() {
	await init()
  return { props: { latestEssays: await getLatestEssays(), meta: await metadata() } }
}

export default function Home({ latestEssays, meta }) {
  const { title, email, gpg } = meta;
  return (
    <Layout title={title} headerLink="/about">
      <Head>
        <title>{title}</title>
      </Head>
      <div className="h-feed">
        <div className="p-name">{title}</div>
        <div className="vcard p-author">
          <div className="org">{title}</div>
          <div className="email">{email}</div>
          <div className="u-url u-uid">https://{title}</div>
          <div className="u-key">{gpg}</div>
        </div>
        <div>
          {latestEssays.map(({ id, date, min, title }) => (
            <div className="h-entry" key={id}>
              <div className="p-name">{title}</div>
              <Link href="/essays/[id]" as={`/essays/${id}`}>
                <a className="u-uid u-url">{title}</a>
              </Link>
              <br />
              <small>
                <Date className="dt-published" dateStr={date} />
                <p>{min} min read</p>
              </small>
            </div>
          ))}
        </div>
      </div>
    </Layout>
  )
}
