import { parseISO, format } from 'date-fns'

export default function Date({ dateStr, dateFmt, className }) {
  const date = parseISO(dateStr)
  const text = format(date, dateFmt || 'LLLL d, yyyy')
  return <time className={className} dateTime={dateStr}>{text}</time>
}
